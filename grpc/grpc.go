package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"app/config"
	"app/genproto/stock_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	// catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	stock_service.RegisterComingServiceServer(grpcServer, service.NewComingService(cfg, log, strg, srvc))
	stock_service.RegisterComingProductServiceServer(grpcServer, service.NewComingProductService(cfg, log, strg, srvc))
	stock_service.RegisterOstatokServiceServer(grpcServer, service.NewOstatokService(cfg, log, strg, srvc))

	// stock_service.RegisterStockProductServiceServer(grpcServer, service.)

	reflection.Register(grpcServer)
	return
}
