package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"app/config"
	"app/genproto/stock_service"

	"app/grpc/client"
	"app/pkg/logger"
	"app/storage"
)

type OstatokService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedOstatokServiceServer
}

func NewOstatokService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OstatokService {
	return &OstatokService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OstatokService) Create(ctx context.Context, req *stock_service.CreateOstatok) (resp *stock_service.Ostatok, err error) {

	i.log.Info("---CreateOstatok------>", logger.Any("req", req))

	pKey, err := i.strg.Ostatok().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOstatok->Ostatok->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Ostatok().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOstatok->Ostatok->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *OstatokService) GetById(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error) {

	c.log.Info("---GetOstatokByID------>", logger.Any("req", req))

	resp, err = c.strg.Ostatok().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetOstatokByID->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OstatokService) GetList(ctx context.Context, req *stock_service.GetListOstatokRequest) (resp *stock_service.GetListOstatokResponse, err error) {

	i.log.Info("---GetOstatok------>", logger.Any("req", req))

	resp, err = i.strg.Ostatok().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OstatokService) Update(ctx context.Context, req *stock_service.UpdateOstatok) (resp *stock_service.Ostatok, err error) {

	i.log.Info("---UpdateOstatok------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Ostatok().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOstatok--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Ostatok().GetByPKey(ctx, &stock_service.OstatokPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OstatokService) Delete(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteOstatok------>", logger.Any("req", req))

	err = i.strg.Ostatok().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}

func (c *OstatokService) GetProduct(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error) {

	c.log.Info("---GetOstatokByProductId------>", logger.Any("req", req))

	resp, err = c.strg.Ostatok().GetProduct(ctx, req)
	if err != nil {
		c.log.Error("!!!GetOstatokByProductId->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
