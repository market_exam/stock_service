
CREATE TABLE IF NOT EXISTS "comings" (
    "id"           UUID PRIMARY KEY,
    "coming_code"   VARCHAR(32),
    "filial_id"    UUID NOT NULL,
    "courier_id"   UUID NOT NULL,
    "date"         TIMESTAMP,
    "status"       VARCHAR(50) not NULL,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "coming_products" (
    "id"           UUID PRIMARY KEY,
    "coming_id"    UUID NOT NULL REFERENCES comings(id),
    "category_id"  UUID NOT NULL,
    "product_id"   UUID NOT NULL,
    "barcode"      VARCHAR(50),
    "count"        INT,
    "coming_price" NUMERIC
);


CREATE TABLE IF NOT EXISTS "ostatoks" (
    "id"           UUID PRIMARY KEY,
    "coming_id"    UUID NOT NULL REFERENCES comings(id),
    "category_id"  UUID NOT NULL,
    "product_id"   UUID NOT NULL,
    "barcode"      VARCHAR (50),
    "count"        INT,
    "coming_price" NUMERIC,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);