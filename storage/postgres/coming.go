package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/cast"

	"app/genproto/stock_service"
	"app/pkg/helper"
	"app/storage"
)

type ComingRepo struct {
	db *pgxpool.Pool
}

func NewComingRepo(db *pgxpool.Pool) storage.ComingRepoI {
	return &ComingRepo{
		db: db,
	}
}

func (c *ComingRepo) Create(ctx context.Context, req *stock_service.CreateComing) (resp *stock_service.ComingPrimaryKey, err error) {

	var idd = uuid.New()
	var id = "M-00000"
	var temp int

	err = c.db.QueryRow(ctx, `SELECT COALESCE(COUNT(*), 0) + 1 FROM comings`).Scan(&temp)
	if err != nil {
		return nil, err
	}

	id += cast.ToString(temp)

	query := `INSERT INTO "comings" (
				id,
				coming_code,
				filial_id,
				courier_id,
				date,
				status,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		idd.String(),
		id,
		req.FilialId,
		req.CourierId,
		req.Date,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ComingPrimaryKey{Id: idd.String()}, nil
}

func (c *ComingRepo) GetByPKey(ctx context.Context, req *stock_service.ComingPrimaryKey) (resp *stock_service.Coming, err error) {

	query := `
		SELECT
			id,
			coming_code,
			filial_id,
			courier_id,
			date,
			status,
			created_at,
			updated_at,
			deleted_at
		FROM "comings"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id          sql.NullString
		coming_code sql.NullString
		filial_id   sql.NullString
		courier_id  sql.NullString
		date        sql.NullString
		status      sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
		deleteddAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&coming_code,
		&filial_id,
		&courier_id,
		&date,
		&status,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Coming{
		Id:         id.String,
		ComingCode: coming_code.String,
		FilialId:   filial_id.String,
		CourierId:  courier_id.String,
		Date:       date.String,
		CreatedAt:  createdAt.String,
		Status:     status.String,
		UpdatedAt:  updatedAt.String,
		DeletedAt:  deleteddAt.String,
	}

	return
}

func (c *ComingRepo) GetAll(ctx context.Context, req *stock_service.GetListComingRequest) (resp *stock_service.GetListComingResponse, err error) {

	resp = &stock_service.GetListComingResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	var courier_id sql.NullString
	var date sql.NullString

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_code,
			filial_id,
			courier_id,
			date,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "comings"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var coming stock_service.Coming

		err := rows.Scan(
			&resp.Count,
			&coming.Id,
			&coming.ComingCode,
			&coming.FilialId,
			&coming.CourierId,
			&coming.Date,
			&coming.Status,
			&coming.CreatedAt,
			&coming.UpdatedAt,
			&coming.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		coming.CourierId = courier_id.String
		coming.Date = date.String

		resp.Comings = append(resp.Comings, &coming)
	}

	return
}

func (c *ComingRepo) Update(ctx context.Context, req *stock_service.UpdateComing) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "comings"
			SET
				coming_code = :coming_code,
				filial_id  = :filial_id,
				courier_id = :courier_id,
				date       = :date,
				status     = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"coming_code": req.GetComingCode(),
		"filial_id":   req.GetFilialId(),
		"courier_id":  req.GetCourierId(),
		"date":        req.GetDate(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ComingRepo) Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) error {

	query := `UPDATE "comings" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.GetId())

	if err != nil {
		return err
	}

	return nil
}
