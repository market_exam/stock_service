package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/stock_service"
	"app/pkg/helper"
	"app/storage"
)

type ComingProductRepo struct {
	db *pgxpool.Pool
}

func NewComingProductRepo(db *pgxpool.Pool) storage.ComingProductRepoI {
	return &ComingProductRepo{
		db: db,
	}
}

func (c *ComingProductRepo) Create(ctx context.Context, req *stock_service.CreateComingProduct) (resp *stock_service.ComingProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "coming_products" (
				id,
				coming_id,
				category_id,
				product_id,
				barcode,
				count,
				coming_price
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ComingId,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.Count,
		req.ComingPrice,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ComingProductPrimaryKey{Id: id.String()}, nil
}

func (c *ComingProductRepo) GetByPKey(ctx context.Context, req *stock_service.ComingProductPrimaryKey) (resp *stock_service.ComingProduct, err error) {

	query := `
		SELECT
			id,
			coming_id,
			category_id,
			product_id,
			barcode,
			count,
			coming_price
		FROM "coming_products"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		coming_id    sql.NullString
		category_id  sql.NullString
		product_id   sql.NullString
		barcode      sql.NullString
		count        sql.NullInt32
		coming_price sql.NullFloat64
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&coming_id,
		&category_id,
		&product_id,
		&barcode,
		&count,
		&coming_price,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.ComingProduct{
		Id:          id.String,
		ComingId:    coming_id.String,
		CategoryId:  category_id.String,
		ProductId:   product_id.String,
		Barcode:     barcode.String,
		Count:       count.Int32,
		ComingPrice: coming_price.Float64,
	}

	return
}

func (c *ComingProductRepo) GetAll(ctx context.Context, req *stock_service.GetListComingProductRequest) (resp *stock_service.GetListComingProductResponse, err error) {

	resp = &stock_service.GetListComingProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
	)

	var coming_id sql.NullString

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			category_id,
			product_id,
			COALESCE(barcode, ''),
			COALESCE(count, 0),
			COALESCE(coming_price, 0)
		FROM "coming_products"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE CAST(coming_id AS VARCHAR) ILIKE " + "'" + req.Search + "'"
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var comingProduct stock_service.ComingProduct

		err := rows.Scan(
			&resp.Count,
			&comingProduct.Id,
			// &coming_id,
			&comingProduct.ComingId,
			&comingProduct.CategoryId,
			&comingProduct.ProductId,
			&comingProduct.Barcode,
			&comingProduct.Count,
			&comingProduct.ComingPrice,
		)

		if err != nil {
			return resp, err
		}

		comingProduct.ComingId = coming_id.String

		resp.ComingProducts = append(resp.ComingProducts, &comingProduct)
	}

	return
}

func (c *ComingProductRepo) Update(ctx context.Context, req *stock_service.UpdateComingProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "coming_products"
			SET
				coming_id = :coming_id,
				category_id = :category_id,
				product_id = :product_id,
				barcode = :barcode,
				count = :count,
				coming_price = :coming_price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"coming_id":    req.GetComingId(),
		"category_id":  req.GetCategoryId(),
		"product_id":   req.GetProductId(),
		"barcode":      req.GetBarcode(),
		"count":        req.GetCount(),
		"coming_price": req.GetComingPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ComingProductRepo) Delete(ctx context.Context, req *stock_service.ComingProductPrimaryKey) error {

	query := `DELETE FROM "coming_products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.GetId())

	if err != nil {
		return err
	}

	return nil
}
