package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/stock_service"
	"app/pkg/helper"
	"app/storage"
)

type OstatokRepo struct {
	db *pgxpool.Pool
}

func NewOstatokRepo(db *pgxpool.Pool) storage.OstatokRepoI {
	return &OstatokRepo{
		db: db,
	}
}

func (c *OstatokRepo) Create(ctx context.Context, req *stock_service.CreateOstatok) (resp *stock_service.OstatokPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "ostatoks" (
				id,
				coming_id,
				category_id,
				product_id,
				barcode,
				count,
				coming_price
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ComingId,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.Count,
		req.ComingPrice,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.OstatokPrimaryKey{Id: id.String()}, nil
}

func (c *OstatokRepo) GetByPKey(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error) {

	query := `
		SELECT
			id,
			coming_id,
			category_id,
			product_id,
			barcode,
			count,
			coming_price
		FROM "ostatoks"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		coming_id    sql.NullString
		category_id  sql.NullString
		product_id   sql.NullString
		barcode      sql.NullString
		count        sql.NullInt32
		coming_price sql.NullFloat64
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&coming_id,
		&category_id,
		&product_id,
		&barcode,
		&count,
		&coming_price,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Ostatok{
		Id:          id.String,
		ComingId:    coming_id.String,
		CategoryId:  category_id.String,
		ProductId:   product_id.String,
		Barcode:     barcode.String,
		Count:       count.Int32,
		ComingPrice: coming_price.Float64,
	}

	return
}

func (c *OstatokRepo) GetAll(ctx context.Context, req *stock_service.GetListOstatokRequest) (resp *stock_service.GetListOstatokResponse, err error) {

	resp = &stock_service.GetListOstatokResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			coming_id,
			category_id,
			product_id,
			COALESCE(barcode, ''),
			COALESCE(count, 0),
			COALESCE(coming_price, 0)
		FROM "ostatoks"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE CAST(coming_id AS VARCHAR) ILIKE " + "'" + req.Search + "'"
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var comingProduct stock_service.Ostatok

		err := rows.Scan(
			&resp.Count,
			&comingProduct.Id,
			// &coming_id,
			&comingProduct.ComingId,
			&comingProduct.CategoryId,
			&comingProduct.ProductId,
			&comingProduct.Barcode,
			&comingProduct.Count,
			&comingProduct.ComingPrice,
		)

		if err != nil {
			return resp, err
		}

		resp.Ostatoks = append(resp.Ostatoks, &comingProduct)
	}

	return
}

func (c *OstatokRepo) Update(ctx context.Context, req *stock_service.UpdateOstatok) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "ostatoks"
			SET
				coming_id = :coming_id,
				category_id = :category_id,
				product_id = :product_id,
				barcode = :barcode,
				count = :count,
				coming_price = :coming_price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"coming_id":    req.GetComingId(),
		"category_id":  req.GetCategoryId(),
		"product_id":   req.GetProductId(),
		"barcode":      req.GetBarcode(),
		"count":        req.GetCount(),
		"coming_price": req.GetComingPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OstatokRepo) Delete(ctx context.Context, req *stock_service.OstatokPrimaryKey) error {

	query := `DELETE FROM "ostatoks" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.GetId())

	if err != nil {
		return err
	}

	return nil
}

func (c *OstatokRepo) GetProduct(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error) {

	query := `
		SELECT
			id,
			coming_id,
			category_id,
			product_id,
			barcode,
			count,
			coming_price
		FROM "ostatoks"
		WHERE product_id = $1
	`

	var (
		id           sql.NullString
		coming_id    sql.NullString
		category_id  sql.NullString
		product_id   sql.NullString
		barcode      sql.NullString
		count        sql.NullInt32
		coming_price sql.NullFloat64
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&coming_id,
		&category_id,
		&product_id,
		&barcode,
		&count,
		&coming_price,
	)

	if err != nil {
		return resp, err
	}

	resp = &stock_service.Ostatok{
		Id:          id.String,
		ComingId:    coming_id.String,
		CategoryId:  category_id.String,
		ProductId:   product_id.String,
		Barcode:     barcode.String,
		Count:       count.Int32,
		ComingPrice: coming_price.Float64,
	}

	return
}
