package postgres

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/config"
	"app/storage"
)

type Store struct {
	db   *pgxpool.Pool
	coming 			storage.ComingRepoI
	comingProduct	storage.ComingProductRepoI
	ostatok			storage.OstatokRepoI
}

type Pool struct {
	db *pgxpool.Pool
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) Coming() storage.ComingRepoI {
	if s.coming == nil {
		s.coming = NewComingRepo(s.db)
	}

	return s.coming
}


func (s *Store) ComingProduct() storage.ComingProductRepoI {
	if s.comingProduct == nil {
		s.comingProduct = NewComingProductRepo(s.db)
	}

	return s.comingProduct
}

func (s *Store) Ostatok() storage.OstatokRepoI {
	if s.ostatok == nil {
		s.ostatok = NewOstatokRepo(s.db)
	}

	return s.ostatok
}