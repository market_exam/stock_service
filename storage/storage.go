package storage

import (
	"app/genproto/stock_service"
	"context"
)

type StorageI interface {
	CloseDB()
	Coming() ComingRepoI
	ComingProduct() ComingProductRepoI
	Ostatok() OstatokRepoI
}

type ComingRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateComing) (resp *stock_service.ComingPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.ComingPrimaryKey) (resp *stock_service.Coming, err error)
	GetAll(ctx context.Context, req *stock_service.GetListComingRequest) (resp *stock_service.GetListComingResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateComing) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) error
}

type ComingProductRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateComingProduct) (resp *stock_service.ComingProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.ComingProductPrimaryKey) (resp *stock_service.ComingProduct, err error)
	GetAll(ctx context.Context, req *stock_service.GetListComingProductRequest) (resp *stock_service.GetListComingProductResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateComingProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.ComingProductPrimaryKey) error
}

type OstatokRepoI interface {
	Create(ctx context.Context, req *stock_service.CreateOstatok) (resp *stock_service.OstatokPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error)
	GetAll(ctx context.Context, req *stock_service.GetListOstatokRequest) (resp *stock_service.GetListOstatokResponse, err error)
	Update(ctx context.Context, req *stock_service.UpdateOstatok) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *stock_service.OstatokPrimaryKey) error
	GetProduct(ctx context.Context, req *stock_service.OstatokPrimaryKey) (resp *stock_service.Ostatok, err error)
}
